# Mailer Microservice

Simple microservice for sending emails built using ASP.NET Core and Entity Framework Core. 
The architecture and design of the project built on JasonGT's Northwind Traders example available under link below:

https://github.com/JasonGT/NorthwindTraders

