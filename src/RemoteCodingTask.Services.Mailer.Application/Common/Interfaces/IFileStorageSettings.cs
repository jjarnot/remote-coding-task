﻿namespace RemoteCodingTask.Services.Mailer.Application.Common.Interfaces
{
    public interface IFileStorageSettings
    {
        string AttachmentsDirectory { get; }
    }
}