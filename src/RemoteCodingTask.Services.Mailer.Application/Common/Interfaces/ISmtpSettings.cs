﻿namespace RemoteCodingTask.Services.Mailer.Application.Common.Interfaces
{
    public interface ISmtpSettings
    {
        string Host { get; }
        int Port { get; }
    }
}
