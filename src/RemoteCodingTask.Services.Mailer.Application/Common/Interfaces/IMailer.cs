﻿using RemoteCodingTask.Services.Mailer.Application.Common.Models;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using RemoteCodingTask.Services.Mailer.Domain.ValueObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RemoteCodingTask.Services.Mailer.Application.Common.Interfaces
{
    public interface IMailer
    {
        Task SendMessage(Email sender, IList<Email> recipients, string subject, string content, MessagePriority priority, IList<AttachmentInfo> attachments);
    }
}