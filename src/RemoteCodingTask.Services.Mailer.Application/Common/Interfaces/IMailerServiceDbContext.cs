﻿using Microsoft.EntityFrameworkCore;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteCodingTask.Services.Mailer.Application.Common.Interfaces
{
    public interface IMailerServiceDbContext
    {
        DbSet<Message> Messages { get; set; }
        void EnsureDatabaseIsCreated();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
