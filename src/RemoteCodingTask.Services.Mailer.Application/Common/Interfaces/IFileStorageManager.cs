﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RemoteCodingTask.Services.Mailer.Application.Common.Interfaces
{
    public interface IFileStorageManager
    {
        void EnsureDirectoryStructureIsCreated();
        void ClearDirectoryStructure();

        IList<string> GetAttachments();
        Task<string> SaveAttachment(IFormFile attachment);
    }
}