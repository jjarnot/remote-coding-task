﻿using System;

namespace RemoteCodingTask.Services.Mailer.Application.Common.Interfaces
{
    public interface ISystemDateTime
    {
        DateTime Now { get; }
        DateTime UtcNow { get; }
    }
}
