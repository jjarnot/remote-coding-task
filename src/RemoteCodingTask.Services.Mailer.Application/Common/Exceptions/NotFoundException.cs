﻿using System;

namespace RemoteCodingTask.Services.Mailer.Application.Common.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string name, Guid key)
            : base($"{name} with id: {key.ToString()} was not found.")
        {
        }
    }
}
