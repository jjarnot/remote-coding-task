﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace RemoteCodingTask.Services.Mailer.Application.Common.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException()
            : base("Validation failed.")
        {
            Errors = new Dictionary<string, string>();
        }

        public ValidationException(List<ValidationFailure> failures)
            : this()
        {
            var propertyNames = failures
                .Select(e => e.PropertyName)
                .Distinct();

            foreach (var propertyName in propertyNames)
            {
                var propertyErrorMessages = failures
                    .Where(e => e.PropertyName == propertyName)
                    .Select(e => e.ErrorMessage);

                Errors.Add(propertyName, propertyErrorMessages.First());
            }
        }

        public IDictionary<string, string> Errors { get; }
    }
}
