﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RemoteCodingTask.Services.Mailer.Application.Common.Models
{
    public class AttachmentInfo
    {
        public string Name { get; private set; }
        public string Path { get; private set; }

        public AttachmentInfo(string name, string path)
        {
            this.Name = name;
            this.Path = path;
        }
    }
}
