﻿using AutoMapper;
using RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessagesList;
using RemoteCodingTask.Services.Mailer.ApplicationMessages.Queries.GetMessageDetail;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using System.Linq;

namespace RemoteCodingTask.Services.Mailer.Application.Common.Mappings
{
    public class DomainToDtoMappingProfile : Profile
    {
        public DomainToDtoMappingProfile()
        {
            CreateMap<Message, MessageDetailsDto>()
                .ForMember(dto => dto.Recipients, map => map.MapFrom(d => string.Join(";", d.Recipients)))
                .ForMember(dto => dto.Status, map => map.MapFrom(d => d.Status.ToString().ToLower()))
                .ForMember(dto => dto.Priority, map => map.MapFrom(d => d.Priority.ToString().ToLower()))
                .ForMember(dto => dto.Attachments, map => map.MapFrom(d => d.Attachments.Select(a => a.Name).ToList()));

            CreateMap<Message, MessageListItemDto>()
                .ForMember(dto => dto.Recipients, map => map.MapFrom(d => string.Join(";", d.Recipients)))
                .ForMember(dto => dto.Status, map => map.MapFrom(d => d.Status.ToString().ToLower()))
                .ForMember(dto => dto.Priority, map => map.MapFrom(d => d.Priority.ToString().ToLower()));
        }
    }
}
