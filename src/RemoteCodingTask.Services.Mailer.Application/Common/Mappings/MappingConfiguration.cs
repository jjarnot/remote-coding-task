﻿using AutoMapper;

namespace RemoteCodingTask.Services.Mailer.Application.Common.Mappings
{
    public class MappingConfiguration
    {
        public MapperConfiguration Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DomainToDtoMappingProfile>();
            });
            return config;
        }
    }
}
