﻿using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using RemoteCodingTask.Services.Mailer.Application.Common.Behaviours;
using System.Reflection;

namespace RemoteCodingTask.Services.Mailer.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            AssemblyScanner
                .FindValidatorsInAssembly(Assembly.GetExecutingAssembly())
                .ForEach(result => services.AddScoped(result.InterfaceType, result.ValidatorType));

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));

            services.AddMediatR(Assembly.GetExecutingAssembly());

            return services;
        }
    }
}
