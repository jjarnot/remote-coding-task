﻿using System;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessagesList
{
    public class MessageListItemDto
    {
        public Guid Id { get; set; }
        public string Sender { get; set; }
        public string Recipients { get; set; }
        public string Subject { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }
    }
}
