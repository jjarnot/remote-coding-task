﻿using MediatR;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessagesList
{
    public class GetMessagesListQuery : IRequest<MessageListDto>
    {

    }
}
