﻿using System.Collections.Generic;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessagesList
{
    public class MessageListDto
    {
        public IList<MessageListItemDto> Messages { get; set; }
    }
}
