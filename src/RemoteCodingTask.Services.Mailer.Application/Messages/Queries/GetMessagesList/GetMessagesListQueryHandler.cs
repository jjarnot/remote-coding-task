﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessagesList
{
    public class GetMessagesListQueryHandler : IRequestHandler<GetMessagesListQuery, MessageListDto>
    {
        private readonly IMailerServiceDbContext context;
        private readonly IMapper mapper;

        public GetMessagesListQueryHandler(IMailerServiceDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<MessageListDto> Handle(GetMessagesListQuery request, CancellationToken cancellationToken)
        {
            var messages = await context.Messages
                .ProjectTo<MessageListItemDto>(mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            var result = new MessageListDto
            {
                Messages = messages
            };

            return result;
        }
    }
}
