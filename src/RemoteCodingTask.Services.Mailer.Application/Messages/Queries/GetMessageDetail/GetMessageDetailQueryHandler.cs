﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RemoteCodingTask.Services.Mailer.Application.Common.Exceptions;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using RemoteCodingTask.Services.Mailer.ApplicationMessages.Queries.GetMessageDetail;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessageDetail
{
    public class GetMessageDetailQueryHandler : IRequestHandler<GetMessageDetailQuery, MessageDetailsDto>
    {
        private readonly IMailerServiceDbContext context;
        private readonly IMapper mapper;

        public GetMessageDetailQueryHandler(IMailerServiceDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<MessageDetailsDto> Handle(GetMessageDetailQuery request, CancellationToken cancellationToken)
        {
            var message = await this.context.Messages
                                .Include(m => m.Attachments)
                                .SingleOrDefaultAsync(m => m.Id == request.Id);

            if (message == null)
            {
                throw new NotFoundException(nameof(Domain.Entities.Message), request.Id);
            }

            return mapper.Map<MessageDetailsDto>(message);
        }
    }
}
