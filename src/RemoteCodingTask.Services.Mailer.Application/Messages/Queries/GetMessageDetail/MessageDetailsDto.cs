﻿using System;
using System.Collections.Generic;

namespace RemoteCodingTask.Services.Mailer.ApplicationMessages.Queries.GetMessageDetail
{
    public class MessageDetailsDto
    {
        public Guid Id { get; set; }
        public string Sender { get; set; }
        public string Recipients { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public IList<string> Attachments { get; set; }
    }
}
