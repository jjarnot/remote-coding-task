﻿using MediatR;
using RemoteCodingTask.Services.Mailer.ApplicationMessages.Queries.GetMessageDetail;
using System;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessageDetail
{
    public class GetMessageDetailQuery : IRequest<MessageDetailsDto>
    {
        public Guid Id { get; set; }
    }
}
