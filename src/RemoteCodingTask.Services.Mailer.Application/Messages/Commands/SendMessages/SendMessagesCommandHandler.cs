﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Commands.SendMessages
{
    public class SendMessagesCommandHandler : IRequestHandler<SendMessagesCommand>
    {
        private readonly IMailerServiceDbContext context;
        private readonly ILogger<SendMessagesCommandHandler> logger;
        private readonly IMailer mailer;

        public SendMessagesCommandHandler(IMailerServiceDbContext context,
            ILogger<SendMessagesCommandHandler> logger,
            IMailer mailer)
        {
            this.context = context;
            this.logger = logger;
            this.mailer = mailer;
        }

        public async Task<Unit> Handle(SendMessagesCommand request, CancellationToken cancellationToken)
        {
            var pendingMessages =   await this.context.Messages
                                    .Include(m => m.Attachments)
                                    .Where(m => m.Status == MessageStatus.Pending)
                                    .ToListAsync();

            if (!pendingMessages.Any())
            {
                this.logger.LogInformation("Send messages skipped because of lack of pending messages.");
                return Unit.Value;
            }

            this.logger.LogInformation($"Fetched {pendingMessages.Count} pending message(s) to send.");

            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            foreach (var pendingMessage in pendingMessages)
            {
                await this.HandleMessage(pendingMessage);
            }

            await this.context.SaveChangesAsync(cancellationToken);

            stopwatch.Stop();
            this.logger.LogInformation($"Send {pendingMessages.Count(s=> s.Status == MessageStatus.Sent)} messages took {stopwatch.Elapsed.TotalSeconds.ToString()} seconds.");

            return Unit.Value;
        }

        private async Task HandleMessage(Message message)
        {
            try
            {
                var attachments = message.Attachments.Select(a => new Common.Models.AttachmentInfo(a.Name, a.Path)).ToList();
                await this.mailer.SendMessage(message.Sender, message.Recipients.ToList(), message.Subject, message.Content, message.Priority, attachments);
                message.UpdateStatus(MessageStatus.Sent);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Send message with id: {message.Id} failed.");
            }
        }
    }
}
