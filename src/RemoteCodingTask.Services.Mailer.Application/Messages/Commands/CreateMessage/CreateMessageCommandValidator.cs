﻿using FluentValidation;
using RemoteCodingTask.Services.Mailer.Domain.Entities;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Commands.CreateMessage
{
    public class CreateMessageCommandValidator : AbstractValidator<CreateMessageCommand>
    {
        public CreateMessageCommandValidator()
        {
            RuleFor(model => model.Sender).NotEmpty().EmailAddress();
            RuleFor(model => model.Recipients).NotEmpty();
            RuleForEach(model => model.Recipients).SetValidator(new EmailAddressValidator());
            RuleFor(model => model.Subject).NotEmpty();
            RuleFor(model => model.Priority).IsEnumName(typeof(MessagePriority), caseSensitive: false);
        }
    }

    public class EmailAddressValidator : AbstractValidator<string>
    {
        public EmailAddressValidator()
        {
            RuleFor(model => model).NotNull().NotEmpty().EmailAddress();
        }
    }
}
