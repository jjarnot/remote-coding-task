﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Commands.CreateMessage
{
    public class EmailAddressList : IEnumerable<string>
    {
        public List<string> Value { get; set; } = new List<string>();

        public EmailAddressList()
        {}

        public EmailAddressList(string emailAddressList)
        {
            Value = Split(emailAddressList);
        }

        public List<string> Split(string emailAddressList, char splitChar = ';')
        {
            return emailAddressList.Contains(splitChar)
                ? emailAddressList.Split(splitChar).ToList()
                : new List<string> { emailAddressList };
        }

        public IEnumerator<string> GetEnumerator()
        {
            return Value.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static implicit operator EmailAddressList(string value)
        {
            return new EmailAddressList(value);
        }

        public string ToString(char splitChar = ';')
        {
            if (Value == null)
                return string.Empty;

            return string.Join(splitChar.ToString(), Value);
        }
    }

    public class CreateMessageCommand : IRequest<Guid>
    {
        public string Sender { get; set; }
        public IList<string> Recipients { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string Priority { get; set; }
        public IList<IFormFile> Attachments { get; set; }
    }
}
