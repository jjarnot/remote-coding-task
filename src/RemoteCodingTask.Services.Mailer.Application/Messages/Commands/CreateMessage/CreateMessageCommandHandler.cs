﻿using MediatR;
using Microsoft.Extensions.Options;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using RemoteCodingTask.Services.Mailer.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace RemoteCodingTask.Services.Mailer.Application.Messages.Commands.CreateMessage
{
    public class CreateMessageCommandHandler : IRequestHandler<CreateMessageCommand, Guid>
    {
        private readonly IMailerServiceDbContext mailerServiceDbContext;
        private readonly IFileStorageManager fileStorageManager;
        private readonly ISystemDateTime systemDateTime;

        public CreateMessageCommandHandler(IMailerServiceDbContext mailerServiceDbContext, 
            IFileStorageManager fileStorageManager,
            ISystemDateTime systemDateTime)
        {
            this.mailerServiceDbContext = mailerServiceDbContext;
            this.fileStorageManager = fileStorageManager;
            this.systemDateTime = systemDateTime;
        }

        public async Task<Guid> Handle(CreateMessageCommand request, CancellationToken cancellationToken)
        {
            var messagePriority = string.IsNullOrEmpty(request.Priority) ? MessagePriority.Normal : (MessagePriority)Enum.Parse(typeof(MessagePriority), request.Priority, true);
            var attachments = await this.ProcessAttachments(request);
            var recipients = new List<string>(request.Recipients).Select(x => new Email(x)).ToList();

            var message = new Message(Guid.NewGuid(),
                                request.Sender,
                                recipients,
                                request.Subject,
                                request.Content,
                                MessageStatus.Pending,
                                systemDateTime.UtcNow,
                                null,
                                messagePriority,
                                attachments);

            this.mailerServiceDbContext.Messages.Add(message);

            await this.mailerServiceDbContext.SaveChangesAsync(cancellationToken);

            return message.Id;
        }

        private async Task<ICollection<Attachment>> ProcessAttachments(CreateMessageCommand request)
        {
            if(request.Attachments is null || request.Attachments.Count == 0)
            {
                return null;
            }

            var result = new HashSet<Attachment>();

            foreach (var file in request.Attachments)
            {
                if (file.Length > 0)
                {
                    var filePath = await fileStorageManager.SaveAttachment(file);
                    result.Add(new Attachment(Guid.NewGuid(), file.FileName, filePath));
                }
            }

            return result;
        }
    }
}