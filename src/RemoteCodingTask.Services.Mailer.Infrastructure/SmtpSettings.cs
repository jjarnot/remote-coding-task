﻿using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;

namespace RemoteCodingTask.Services.Mailer.Infrastructure
{
    public class SmtpSettings : ISmtpSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
