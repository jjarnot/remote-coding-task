﻿using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using System;

namespace RemoteCodingTask.Services.Mailer.Infrastructure
{
    public class SystemDateTime : ISystemDateTime
    {
        public DateTime Now => DateTime.Now;

        public DateTime UtcNow => DateTime.UtcNow;
    }
}
