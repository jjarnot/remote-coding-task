﻿using MailKit.Net.Smtp;
using MimeKit;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using RemoteCodingTask.Services.Mailer.Application.Common.Models;
using RemoteCodingTask.Services.Mailer.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MessagePriority = RemoteCodingTask.Services.Mailer.Domain.Entities.MessagePriority;

namespace RemoteCodingTask.Services.Mailer.Infrastructure
{
    public class Mailer : IMailer
    {
        private readonly ISmtpSettings smtpSettings;

        public Mailer(ISmtpSettings smtpSettings)
        {
            this.smtpSettings = smtpSettings;
        }

        public async Task SendMessage(Email sender, IList<Email> recipients, string subject, string content, MessagePriority priority, IList<AttachmentInfo> attachments)
        {
            if (sender is null)
                throw new ArgumentNullException(nameof(sender));

            if (recipients is null)
                throw new ArgumentNullException(nameof(recipients));

            if (subject is null)
                throw new ArgumentNullException(nameof(subject));

            var mimeMessage = new MimeMessage();

            mimeMessage.From.Add(new MailboxAddress(sender));
            mimeMessage.To.AddRange(this.BuildMessageTo(recipients));
            mimeMessage.Subject = subject;
            mimeMessage.Priority = this.ConvertToMimeKitMessagePriority(priority);
            mimeMessage.Body = this.BuildMessageBody(content, attachments);

            using (var smtpClient = new SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                smtpClient.ServerCertificateValidationCallback = (s, c, h, e) => true;

                smtpClient.Connect(this.smtpSettings.Host, this.smtpSettings.Port, false);
                // client.Authenticate("user_name_here", "pwd_here");

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");

                await smtpClient.SendAsync(mimeMessage);
                smtpClient.Disconnect(true);
            }
        }

        private IEnumerable<InternetAddress> BuildMessageTo(IList<Email> recipients)
        {
            var result = new List<MailboxAddress>();

            foreach (var recipientEmail in recipients)
            {
                if (!string.IsNullOrWhiteSpace(recipientEmail))
                {
                    result.Add(new MailboxAddress(recipientEmail));
                }
            }

            return result;
        }

        private MimeKit.MessagePriority ConvertToMimeKitMessagePriority(MessagePriority messagePriority)
        {
            switch (messagePriority)
            {
                case MessagePriority.High:
                    return MimeKit.MessagePriority.Urgent;
                case MessagePriority.Low:
                    return MimeKit.MessagePriority.NonUrgent;
                case MessagePriority.Normal:
                    return MimeKit.MessagePriority.Normal;
                default:
                    throw new NotImplementedException($"Unrecognized MessagePriority value: {messagePriority}");
            }
        }

        private MimeEntity BuildMessageBody(string content, IList<AttachmentInfo> attachments)
        {
            var bodyBuilder = new BodyBuilder() { TextBody = content };

            if (attachments?.Any() ?? false)
            {
                foreach (var item in attachments)
                {
                    bodyBuilder.Attachments.Add(item.Name, System.IO.File.ReadAllBytes(item.Path));
                }
            }

            return bodyBuilder.ToMessageBody();
        }
    }
}
