﻿using Microsoft.AspNetCore.Http;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RemoteCodingTask.Services.Mailer.Infrastructure
{
    public class FileStorageManager : IFileStorageManager
    {
        private readonly IFileStorageSettings fileStorageSettings;

        public FileStorageManager(IFileStorageSettings fileStorageSettings)
        {
            this.fileStorageSettings = fileStorageSettings;
        }

        private string GenerateAttachmentPath(string attachmentName)
        {
            return Path.Combine(fileStorageSettings.AttachmentsDirectory, String.Concat(Guid.NewGuid().ToString(), "-", attachmentName));
        }

        public void EnsureDirectoryStructureIsCreated()
        {
            if (String.IsNullOrWhiteSpace(fileStorageSettings.AttachmentsDirectory))
            {
                throw new InvalidOperationException("Invalid attachment directory of file storage settings");
            }

            if (!Directory.Exists(fileStorageSettings.AttachmentsDirectory))
            {
                Directory.CreateDirectory(fileStorageSettings.AttachmentsDirectory);
            }
        }

        public void ClearDirectoryStructure()
        {
            if (Directory.Exists(fileStorageSettings.AttachmentsDirectory))
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(fileStorageSettings.AttachmentsDirectory);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }
        }

        public IList<string> GetAttachments()
        {
            return Directory.GetFiles(fileStorageSettings.AttachmentsDirectory).ToList();
        }


        public async Task<string> SaveAttachment(IFormFile attachment)
        {
            var filePath = this.GenerateAttachmentPath(attachment.FileName);

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await attachment.CopyToAsync(fileStream);
            }

            return filePath;
        }
    }
}
