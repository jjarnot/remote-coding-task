﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;

namespace RemoteCodingTask.Services.Mailer.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ISystemDateTime, SystemDateTime>();
            services.AddTransient<IMailer, Mailer>();
            services.AddTransient<IFileStorageManager, FileStorageManager>();

            services.Configure<SmtpSettings>(configuration.GetSection("Smtp"));
            services.Configure<FileStorageSettings>(configuration.GetSection("FileStorage"));

            services.AddSingleton<ISmtpSettings>(sp => sp.GetRequiredService<IOptions<SmtpSettings>>().Value);
            services.AddSingleton<IFileStorageSettings>(sp => sp.GetRequiredService<IOptions<FileStorageSettings>>().Value);

            return services;
        }
    }
}
