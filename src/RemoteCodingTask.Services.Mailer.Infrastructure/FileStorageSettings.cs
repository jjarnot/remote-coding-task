﻿using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;

namespace RemoteCodingTask.Services.Mailer.Infrastructure
{
    public class FileStorageSettings : IFileStorageSettings
    {
        public string AttachmentsDirectory { get; set; }
    }
}
