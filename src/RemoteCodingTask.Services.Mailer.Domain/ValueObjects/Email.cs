﻿using RemoteCodingTask.Services.Mailer.Domain.Common;
using System;
using System.Collections.Generic;

namespace RemoteCodingTask.Services.Mailer.Domain.ValueObjects
{
    public class Email : ValueObject
    {
        public string Value { get; private set; }

        public Email(string value)
        {
            if (!this.IsValid(value))
            {
                throw new ArgumentException("Not valid email", nameof(value));
            }

            Value = value;
        }

        public override string ToString() => Value;

        public static implicit operator string(Email value)
        {
            return value.Value;
        }

        public static implicit operator Email(string value)
        {
            return new Email(value);
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
        }

        // based on: 
        // https://stackoverflow.com/questions/1365407/c-sharp-code-to-validate-email-address
        private bool IsValid(string value)
        {
            try
            {
                var mailAddress = new System.Net.Mail.MailAddress(value);
                return mailAddress.Address == value;
            }
            catch
            {
                return false;
            }
        }
    }
}
