﻿using System;

namespace RemoteCodingTask.Services.Mailer.Domain.Common
{
    public abstract class AuditableEntity
    {
        public DateTime CreatedAt { get; private set; }

        public DateTime? ModifiedAt { get; private set; }

        public void UpdateCreatedAt(DateTime newDate)
        {
            this.CreatedAt = newDate;
        }

        public void UpdateModifiedAt(DateTime newDate)
        {
            this.ModifiedAt = newDate;
        }
    }
}
