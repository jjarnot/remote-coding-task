﻿using RemoteCodingTask.Services.Mailer.Domain.Common;
using RemoteCodingTask.Services.Mailer.Domain.ValueObjects;
using System;
using System.Collections.Generic;

namespace RemoteCodingTask.Services.Mailer.Domain.Entities
{
    public class Message : AuditableEntity
    {
        private Message() { }

        public Message(Guid id,
                Email sender,
                ICollection<Email> recipients, 
                string subject, 
                string content, 
                MessageStatus status, 
                DateTime createdAt,
                DateTime? modifiedAt = null,
                MessagePriority priority = MessagePriority.Normal, 
                ICollection<Attachment> attachments = null)
        {
            if (id == default)
                throw new ArgumentException("Identity must be specified", nameof(id));

            if (recipients.Count == 0)
                throw new ArgumentException("At least one recipient must be specified", nameof(recipients));

            if (subject is null)
                throw new ArgumentNullException(nameof(subject));

            if (subject == string.Empty)
                throw new ArgumentException("Subject cannot be empty", nameof(subject));

            this.Id = id;
            this.Sender = sender;
            this.Recipients = recipients;
            this.Subject = subject;
            this.Content = content;
            this.Status = status;
            this.UpdateCreatedAt(createdAt);

            if(modifiedAt != null) { 
                this.UpdateModifiedAt(modifiedAt.Value);
            }
            this.Priority = priority;
            this.Attachments = attachments ?? new HashSet<Attachment>();
        }

        public Guid Id { get; private set; }
        public Email Sender { get; private set; }
        public ICollection<Email> Recipients {get; private set; }
        public string Subject { get; private set; }
        public string Content { get; private set; }
        public MessageStatus Status { get; private set; }
        public MessagePriority Priority { get; private set; }
        public ICollection<Attachment> Attachments { get; private set; }

        public void UpdateStatus(MessageStatus status)
        {
            this.Status = status;
        }
    }
}
