﻿namespace RemoteCodingTask.Services.Mailer.Domain.Entities
{
    public enum MessageStatus
    {
        Pending,
        Sent
    }
}
