﻿using System;

namespace RemoteCodingTask.Services.Mailer.Domain.Entities
{
    public class Attachment
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public string Path { get; private set; }

        private Attachment() { }

        public Attachment(Guid id, string name, string path)
        {
            if (id == default)
                throw new ArgumentException("Identity must be specified", nameof(id));

            if (name is null)
                throw new ArgumentNullException(nameof(name));

            if (name == string.Empty)
                throw new ArgumentException("Name cannot be empty",nameof(name));

            if (path is null)
                throw new ArgumentNullException(nameof(path));

            if (path == string.Empty)
                throw new ArgumentException("Path cannot be empty", nameof(path));

            this.Id = id;
            this.Name = name;
            this.Path = path;
        }
    }
}
