﻿namespace RemoteCodingTask.Services.Mailer.Domain.Entities
{
    public enum MessagePriority
    {
        Low,
        Normal,
        High
    }
}
