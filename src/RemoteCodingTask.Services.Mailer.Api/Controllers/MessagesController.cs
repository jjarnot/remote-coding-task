﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using MediatR;
using RemoteCodingTask.Services.Mailer.Application.Messages.Commands.CreateMessage;
using RemoteCodingTask.Services.Mailer.Application.Messages.Commands.SendMessages;
using RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessageDetail;
using RemoteCodingTask.Services.Mailer.ApplicationMessages.Queries.GetMessageDetail;
using RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessagesList;
using System.Text.RegularExpressions;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace RemoteCodingTask.Services.Mailer.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly IMediator mediator;
        private readonly LinkGenerator linkGenerator;

        public MessagesController(IMediator mediator, LinkGenerator linkGenerator)
        {
            this.mediator = mediator;
            this.linkGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<MessageListItemDto>> Get()
        {
            return Ok(await this.mediator.Send(new GetMessagesListQuery()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MessageDetailsDto>> Get(Guid id)
        {
            return Ok(await this.mediator.Send(new GetMessageDetailQuery { Id = id }));
        }

        [HttpPost]
        [Consumes("multipart/form-data")]
        public async Task<ActionResult> Post([FromForm]CreateMessageCommand command)
        {
            // swagger issue fix
            // info :: https://stackoverflow.com/questions/56542429/send-string-array-with-formdata-in-swagger-c-sharp-net-core
            if(command.Recipients?.Count == 1) { 
                command.Recipients = Regex.Replace(command.Recipients.FirstOrDefault(), "\"", string.Empty).Split(",", StringSplitOptions.None);
            }

            var messageId = await this.mediator.Send(command);
            var location = this.linkGenerator.GetPathByAction(HttpContext, "Get", values: new { id = messageId });

            return this.Created(location, messageId);
        }

        [HttpPost("send")]
        public async Task<ActionResult> Send()
        {
            var command = new SendMessagesCommand();
            await this.mediator.Send(command);

            return this.NoContent();
        }

        [HttpOptions]
        public IActionResult Options()
        {
            var supportedMethods = new string[] { 
                                        HttpMethod.Get.ToString(),
                                        HttpMethod.Options.ToString(), 
                                        HttpMethod.Post.ToString()
                                    };
            Response.Headers.Add(HttpRequestHeader.Allow.ToString(), string.Join(',', supportedMethods));
            
            return this.Ok();
        }
    }
}
