﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;

namespace RemoteCodingTask.Services.Mailer.Api.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder EnsureDatabaseIsCreated(this IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<IMailerServiceDbContext>();
                context.EnsureDatabaseIsCreated();
            }

            return applicationBuilder;
        }

        public static IApplicationBuilder EnsureDirectoryStructureIsCreated(this IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var fileStorageManager = serviceScope.ServiceProvider.GetRequiredService<IFileStorageManager>();
                fileStorageManager.EnsureDirectoryStructureIsCreated();
            }

            return applicationBuilder;
        }
    }
}
