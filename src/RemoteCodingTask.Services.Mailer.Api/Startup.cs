using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RemoteCodingTask.Services.Mailer.Application;
using RemoteCodingTask.Services.Mailer.Infrastructure;
using RemoteCodingTask.Services.Mailer.Persistence;
using RemoteCodingTask.Services.Mailer.Api.Middlewares;
using Microsoft.AspNetCore.Routing;
using RemoteCodingTask.Services.Mailer.Application.Common.Mappings;
using System.Reflection;
using AutoMapper;
using RemoteCodingTask.Services.Mailer.Api.Extensions;
using Microsoft.Extensions.Logging;

namespace RemoteCodingTask.Services.Mailer.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddInfrastructure(Configuration);
            services.AddPersistence(Configuration);
            services.AddApplication();

            services.AddHealthChecks()
                    .AddDbContextCheck<MailerServiceDbContext>();

            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);

            services.AddAutoMapper(new Assembly[] { typeof(MappingConfiguration).GetTypeInfo().Assembly });
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Mailer API";
                    document.Info.Description = "A simple microservice for sending emails written in ASP.NET Core";
                };
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggetFactory)
        {
            loggetFactory.AddFile(this.Configuration.GetSection("Logging"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCustomExceptionHandler();
            app.UseHealthChecks("/health");
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.EnsureDatabaseIsCreated();
            app.EnsureDirectoryStructureIsCreated(); 
        }
    }
}
