﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;

namespace RemoteCodingTask.Services.Mailer.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<MailerServiceDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("MailerServiceDatabase")));

            services.AddScoped<IMailerServiceDbContext>(provider => provider.GetService<MailerServiceDbContext>());

            return services;
        }
    }
}
