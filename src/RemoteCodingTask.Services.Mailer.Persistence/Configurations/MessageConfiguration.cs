﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using System;
using System.Linq;

namespace RemoteCodingTask.Services.Mailer.Persistence.Configurations
{
    public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            // An email address must not exceed 254 characters.
            // source : https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address/44317754
            builder.Property(e => e.Sender)
                    .HasConversion(
                        v => v.ToString(),
                        v => new Domain.ValueObjects.Email(v)
                    )
                    .IsRequired()
                    .HasMaxLength(254);

            builder.Property(e => e.Recipients)
                    .HasConversion(
                        v => string.Join(';', v),
                        v => v.Split(';', StringSplitOptions.RemoveEmptyEntries).Select(x => new Domain.ValueObjects.Email(x)).ToList()
                    )
                    .IsRequired();

            builder.Property(e => e.Subject)
                .IsRequired();

            builder.Property(e => e.Content);

            builder.Property(e => e.CreatedAt)
                .IsRequired()
                .HasColumnType("datetime");

            builder.Property(e => e.ModifiedAt)
                .HasColumnType("datetime");

            builder.Property(e => e.Status)
                .IsRequired();
        }
    }
}
