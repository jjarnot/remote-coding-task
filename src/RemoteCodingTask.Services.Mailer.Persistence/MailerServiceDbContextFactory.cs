﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace RemoteCodingTask.Services.Mailer.Persistence
{
    public class MailerServiceDbContextFactory : DesignTimeDbContextFactoryBase<MailerServiceDbContext>
    {
        protected override MailerServiceDbContext CreateNewInstance(DbContextOptions<MailerServiceDbContext> options)
        {
            return new MailerServiceDbContext(options);
        }
    }
}
