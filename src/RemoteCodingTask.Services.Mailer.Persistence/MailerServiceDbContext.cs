﻿using Microsoft.EntityFrameworkCore;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using RemoteCodingTask.Services.Mailer.Domain.Common;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteCodingTask.Services.Mailer.Persistence
{
    public class MailerServiceDbContext : DbContext, IMailerServiceDbContext
    {
        private readonly ISystemDateTime systemDateTime;

        public MailerServiceDbContext(DbContextOptions options) : base(options)
        {
        }

        public MailerServiceDbContext(DbContextOptions options, ISystemDateTime systemDateTime) : base(options)
        {
            this.systemDateTime = systemDateTime;
        }

        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Message> Messages { get; set; }

        public void EnsureDatabaseIsCreated()
        {
            this.Database.EnsureCreated();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.UpdateCreatedAt(systemDateTime.UtcNow);
                        break;
                    case EntityState.Modified:
                        entry.Entity.UpdateModifiedAt(systemDateTime.UtcNow);
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(MailerServiceDbContext).Assembly);
        }
    }
}
