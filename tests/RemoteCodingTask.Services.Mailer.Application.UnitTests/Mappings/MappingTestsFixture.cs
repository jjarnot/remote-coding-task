﻿using AutoMapper;
using RemoteCodingTask.Services.Mailer.Application.Common.Mappings;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Common.Mappings
{
    public class MappingTestsFixture
    {
        public MappingTestsFixture()
        {
            this.ConfigurationProvider = new MappingConfiguration().Configure();
            this.Mapper = ConfigurationProvider.CreateMapper();
        }

        public IConfigurationProvider ConfigurationProvider { get; }

        public IMapper Mapper { get; }
    }
}
