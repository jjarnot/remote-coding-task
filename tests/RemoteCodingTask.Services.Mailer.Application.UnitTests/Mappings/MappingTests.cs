﻿using AutoMapper;
using RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessagesList;
using RemoteCodingTask.Services.Mailer.ApplicationMessages.Queries.GetMessageDetail;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using RemoteCodingTask.Services.Mailer.Domain.ValueObjects;
using Shouldly;
using System;
using System.Collections.Generic;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Common.Mappings
{
    public class MappingTests : IClassFixture<MappingTestsFixture>
    {
        private readonly IConfigurationProvider configuration;
        private readonly IMapper mapper;

        public MappingTests(MappingTestsFixture fixture)
        {
            this.configuration = fixture.ConfigurationProvider;
            this.mapper = fixture.Mapper;
        }

        [Fact]
        public void ShouldHaveValidConfiguration()
        {
            this.configuration.AssertConfigurationIsValid();
        }

        [Fact]
        public void ShouldMapMessageToMessageDetailsDto()
        {
            var entity = new Message(Guid.NewGuid(),
                                    "sender@localhost", 
                                    new List<Email>() { "receiver@localhost" },
                                    "subject",
                                    "content",
                                    MessageStatus.Pending,
                                    new DateTime(2020,1,1));

            var dto = this.mapper.Map<MessageDetailsDto>(entity);

            dto.ShouldNotBeNull();
            dto.ShouldBeOfType<MessageDetailsDto>();
            dto.Id.ShouldBe(entity.Id);
        }

        [Fact]
        public void ShouldMapMessageToMessageListItemDto()
        {
            var entity = new Message(Guid.NewGuid(),
                                    "sender@localhost",
                                    new List<Email>() { "receiver@localhost" },
                                    "subject",
                                    "content",
                                    MessageStatus.Pending,
                                    new DateTime(2020, 1, 1));

            var dto = this.mapper.Map<MessageListItemDto>(entity);

            dto.ShouldNotBeNull();
            dto.ShouldBeOfType<MessageListItemDto>();
            dto.Id.ShouldBe(entity.Id);
        }
    }
}
