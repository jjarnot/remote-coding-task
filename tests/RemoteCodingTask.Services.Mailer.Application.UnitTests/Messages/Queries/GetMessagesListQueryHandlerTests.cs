﻿using AutoMapper;
using RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessagesList;
using RemoteCodingTask.Services.Mailer.Application.UnitTests.Common;
using RemoteCodingTask.Services.Mailer.Application.UnitTests.Data;
using RemoteCodingTask.Services.Mailer.Persistence;
using Shouldly;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Messages.Queries
{
    [Collection("QueryCollection")]
    public class GetMessagesListQueryHandlerTests
    {
        private readonly MailerServiceDbContext context;
        private readonly IMapper mapper;

        public GetMessagesListQueryHandlerTests(QueryTestFixture fixture)
        {
            context = fixture.Context;
            mapper = fixture.Mapper;
        }

        [Fact]
        public async Task ShouldGetMessagesList()
        {
            var sut = new GetMessagesListQueryHandler(context, mapper);

            var result = await sut.Handle(new GetMessagesListQuery(), CancellationToken.None);

            result.ShouldBeOfType<MessageListDto>();
            result.Messages.Count.ShouldBe(SeedData.Messages.Count);

            foreach (var item in result.Messages)
            {
                SeedData.Messages.Count(m => m.Id == item.Id).ShouldBe(1);
            }
        }
    }
}
