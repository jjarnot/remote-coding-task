﻿using AutoMapper;
using RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessageDetail;
using RemoteCodingTask.Services.Mailer.Application.UnitTests.Common;
using RemoteCodingTask.Services.Mailer.Application.UnitTests.Data;
using RemoteCodingTask.Services.Mailer.ApplicationMessages.Queries.GetMessageDetail;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using RemoteCodingTask.Services.Mailer.Persistence;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Messages.Queries
{
    [Collection("QueryCollection")]
    public class GetMessageDetailQueryHandlerTests
    {
        private readonly MailerServiceDbContext context;
        private readonly IMapper mapper;

        public GetMessageDetailQueryHandlerTests(QueryTestFixture fixture)
        {
            context = fixture.Context;
            mapper = fixture.Mapper;
        }

        [Theory]
        [ClassData(typeof(SeedData))]
        public async Task ShouldGetMessageDetail(Message message)
        {
            var sut = new GetMessageDetailQueryHandler(context, mapper);

            var result = await sut.Handle(new GetMessageDetailQuery() { Id = message.Id}, CancellationToken.None);

            result.ShouldBeOfType<MessageDetailsDto>();
            result.Id.ShouldBe(message.Id);
        }
    }
}
