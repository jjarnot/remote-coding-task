﻿using FluentValidation.TestHelper;
using RemoteCodingTask.Services.Mailer.Application.Messages.Commands.CreateMessage;
using RemoteCodingTask.Services.Mailer.Application.UnitTests.Data;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Messages.Commands.CreateMessage
{
    public class CreateMessageCommandValidatorTests
    {
        private readonly CreateMessageCommandValidator validator = new CreateMessageCommandValidator();

        [Fact]
        public void ShouldHaveValidationErrorWhenSenderIsNull()
        {
            validator.ShouldHaveValidationErrorFor(x => x.Sender, new CreateMessageCommand()
            {
                Sender = null
            });
        }

        [Fact]
        public void ShouldHaveValidationErrorWhenSenderIsEmpty()
        {
            validator.ShouldHaveValidationErrorFor(x => x.Sender, new CreateMessageCommand()
            {
                Sender = string.Empty
            });
        }

        [Fact]
        public void ShouldHaveValidationErrorWhenSenderIsWhiteSpace()
        {
            validator.ShouldHaveValidationErrorFor(x => x.Sender, new CreateMessageCommand()
            {
                Sender = " "
            });
        }

        [Theory]
        [ClassData(typeof(InvalidEmailsData))]
        public void ShouldHaveValidationErrorWhenSenderIsNotValidEmail(string email)
        {
            validator.ShouldHaveValidationErrorFor(x => x.Sender, new CreateMessageCommand()
            {
                Sender = email
            });
        }

        [Theory]
        [ClassData(typeof(ValidEmailsData))]
        public void ShouldNotHaveValidationErrorWhenSenderIsNotValidEmail(string email)
        {
            validator.ShouldNotHaveValidationErrorFor(x => x.Sender, new CreateMessageCommand()
            {
                Sender = email
            });
        }

        [Fact]
        public void ShouldHaveValidationErrorWhenRecipientsValueIsNull()
        {
            validator.ShouldHaveValidationErrorFor(x => x.Recipients, new CreateMessageCommand()
            {
                Recipients = null
            });
        }

        [Fact]
        public void ShouldHaveValidationErrorWhenRecipientsValueIsEmptyList()
        {
            validator.ShouldHaveValidationErrorFor(x => x.Recipients, new CreateMessageCommand()
            {
                Recipients = new List<string>()
            });
        }

        [Theory]
        [ClassData(typeof(InvalidEmailsData))]
        public void ShouldHaveValidationErrorWhenRecipientsValueIsListWithOneInvalidEmail(string email)
        {
            validator.ShouldHaveValidationErrorFor(x => x.Recipients, new CreateMessageCommand()
            {
                Recipients = new List<string>() { email }
            });
        }

        [Theory]
        [ClassData(typeof(ValidEmailsData))]
        public void ShouldNotHaveValidationErrorWhenRecipientsValueIsListWithOneValidEmail(string email)
        {
            validator.ShouldNotHaveValidationErrorFor(x => x.Recipients, new CreateMessageCommand()
            {
                Recipients = new List<string>() { email}
            });
        }

        [Fact]
        public void ShouldNotHaveValidationErrorWhenRecipientsValueIsListWithValidEmails()
        {
            validator.ShouldNotHaveValidationErrorFor(x => x.Recipients, new CreateMessageCommand()
            {
                Recipients = new ValidEmailsData().Select(o => o[0].ToString()).ToList()
            });
        }

        [Theory]
        [ClassData(typeof(InvalidEmailsData))]
        public void ShouldHaveValidationErrorWhenRecipientsValueIsListWithAtLeastOneInvalidEmail(string invalidEmail)
        {
            var emails = new ValidEmailsData().Select(o => o[0].ToString()).ToList();
            emails.Add(invalidEmail);

            validator.ShouldHaveValidationErrorFor(x => x.Recipients, new CreateMessageCommand()
            {
                Recipients = emails
            });
        }

        [Fact]
        public void ShouldHaveValidationErrorWhenSubjectIsNull()
        {
            validator.ShouldHaveValidationErrorFor(x => x.Subject, new CreateMessageCommand()
            {
                Subject = null
            });
        }

        [Fact]
        public void ShouldHaveValidationErrorWhenSubjectIsEmpty()
        {
            validator.ShouldHaveValidationErrorFor(x => x.Subject, new CreateMessageCommand()
            {
                Subject = string.Empty
            });
        }

        [Fact]
        public void ShouldNotHaveValidationErrorWhenSubjectIsNotEmpty()
        {
            validator.ShouldNotHaveValidationErrorFor(x => x.Subject, new CreateMessageCommand()
            {
                Subject = "some text"
            });
        }

        [Theory]
        [ClassData(typeof(InvalidMessagePriorityData))]
        public void ShouldHaveValidationErrorWhenSenderIsNotValidPriority(string priority)
        {
            validator.ShouldHaveValidationErrorFor(x => x.Priority, new CreateMessageCommand()
            {
                Priority = priority
            });
        }

        [Theory]
        [ClassData(typeof(ValidMessagePriorityData))]
        public void ShouldNotHaveValidationErrorWhenSenderIsNotValidPriority(string priority)
        {
            validator.ShouldNotHaveValidationErrorFor(x => x.Priority, new CreateMessageCommand()
            {
                Priority = priority
            });
        }
    }
}
