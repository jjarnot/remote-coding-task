﻿using RemoteCodingTask.Services.Mailer.Domain.Entities;
using RemoteCodingTask.Services.Mailer.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Data
{
    public static class PredefinedData
    {
        public static readonly DateTime UtcNow = new DateTime(2020, 1, 1);

        public static readonly Message PendingMessageWithOneRecipientAndNormalPriority
            = new Message(Guid.NewGuid(), "s@example.com", new List<Email>() { "r1@example.com" }, "subject-1", "content-1",
                          MessageStatus.Pending, PredefinedData.UtcNow.AddDays(-1), null, MessagePriority.Normal);

        public static readonly Message PendingMessageWithTwoRecipientsAndNormalPriority
            = new Message(Guid.NewGuid(), "s@example.com", new List<Email>() { "r1@example.com", "r2@example.com" }, "subject-2", "content-2",
                          MessageStatus.Pending, PredefinedData.UtcNow.AddDays(-1), null, MessagePriority.Normal);

        public static readonly Message SentMessageWithOneRecipientAndLowPriority
            = new Message(Guid.NewGuid(), "s@example.com", new List<Email>() { "r1@example.com" }, "subject-6", "content-6",
                          MessageStatus.Sent, PredefinedData.UtcNow.AddDays(-1), PredefinedData.UtcNow.AddMinutes(-10), MessagePriority.Low);
    }
}
