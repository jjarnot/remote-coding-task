﻿using RemoteCodingTask.Services.Mailer.Domain.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Data
{
    public class SeedData : IEnumerable<object[]>
    {
        public static readonly IList<Message> Messages = new List<Message>{
            PredefinedData.PendingMessageWithOneRecipientAndNormalPriority,
            PredefinedData.PendingMessageWithTwoRecipientsAndNormalPriority,
            PredefinedData.SentMessageWithOneRecipientAndLowPriority
        };

        public IEnumerator<object[]> GetEnumerator() => Messages.Select(m => new object[] { m}).ToList().GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
