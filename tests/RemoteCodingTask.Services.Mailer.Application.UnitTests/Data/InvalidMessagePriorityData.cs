﻿using RemoteCodingTask.Services.Mailer.Domain.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Data
{
    public class InvalidMessagePriorityData : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] { "123" },
            new object[] { "  " },
            new object[] { "_high" },
            new object[] { "high_" },
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
