﻿using RemoteCodingTask.Services.Mailer.Domain.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Data
{
    public class ValidMessagePriorityData : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] { MessagePriority.High.ToString() },
            new object[] { MessagePriority.High.ToString().ToLower() },
            new object[] { MessagePriority.Normal.ToString() },
            new object[] { MessagePriority.Normal.ToString().ToLower() },
            new object[] { MessagePriority.Low.ToString() },
            new object[] { MessagePriority.Low.ToString().ToLower() },
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
