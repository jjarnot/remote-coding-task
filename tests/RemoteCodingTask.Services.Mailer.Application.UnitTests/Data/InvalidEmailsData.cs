﻿using System.Collections;
using System.Collections.Generic;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Data
{
    public class InvalidEmailsData : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] { "someaddress" },
            new object[] { "email@-example.com" },
            new object[] { "Abc..123@example.net" },
            new object[] { "@example.org" },
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
