﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Data
{
    public class ValidEmailsData : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[] { "email@example.com" },
            new object[] { "firstname-lastname@example.com" },
            new object[] { "firstname.lastname@example.com" },
            new object[] { "email@subdomain.example.com" },
            new object[] { "firstname+lastname@example.com" },
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
