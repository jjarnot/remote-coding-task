﻿using Microsoft.EntityFrameworkCore;
using RemoteCodingTask.Services.Mailer.Application.UnitTests.Data;
using RemoteCodingTask.Services.Mailer.Persistence;
using System;
using System.Collections.Generic;
using System.Text;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Common
{
    public class MailerServiceDbContextFactory
    {
        public static MailerServiceDbContext Create()
        {
            var options = new DbContextOptionsBuilder<MailerServiceDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new MailerServiceDbContext(options);

            context.Database.EnsureCreated();

            context.Messages.AddRange(SeedData.Messages);

            context.SaveChanges();

            return context;
        }

        public static void Destroy(MailerServiceDbContext context)
        {
            context.Database.EnsureDeleted();

            context.Dispose();
        }
    }
}
