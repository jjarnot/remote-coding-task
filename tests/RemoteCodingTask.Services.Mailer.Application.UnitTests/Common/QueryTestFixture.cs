﻿using AutoMapper;
using RemoteCodingTask.Services.Mailer.Application.Common.Mappings;
using RemoteCodingTask.Services.Mailer.Persistence;
using System;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Application.UnitTests.Common
{
    public class QueryTestFixture : IDisposable
    {
        public MailerServiceDbContext Context { get; private set; }
        public IMapper Mapper { get; private set; }

        public QueryTestFixture()
        {
            Context = MailerServiceDbContextFactory.Create();

            var configurationProvider = new MappingConfiguration().Configure();

            this.Mapper = configurationProvider.CreateMapper();
        }

        public void Dispose()
        {
            MailerServiceDbContextFactory.Destroy(Context);
        }
    }

    [CollectionDefinition("QueryCollection")]
    public class QueryCollection : ICollectionFixture<QueryTestFixture> { }
}
