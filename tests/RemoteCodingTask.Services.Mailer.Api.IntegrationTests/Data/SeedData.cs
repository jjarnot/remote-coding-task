﻿using RemoteCodingTask.Services.Mailer.Domain.Entities;
using System.Collections.Generic;

namespace RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Data
{
    public static class SeedData
    {
        public static readonly IEnumerable<Message> Messages = new List<Message>{
            PredefinedData.PendingMessageWithOneRecipientAndNormalPriority,
            PredefinedData.PendingMessageWithTwoRecipientsAndNormalPriority,
            PredefinedData.PendingMessageWithOneRecipientAndHighPriority,
            PredefinedData.SentMessageWithOneRecipientAndNormalPriority,
            PredefinedData.SentMessageWithTwoRecipientsAndNormalPriority,
            PredefinedData.SentMessageWithOneRecipientAndLowPriority
        };
    }
}
