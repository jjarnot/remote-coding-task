﻿using System.Collections.Generic;
using System.Net.Http;

namespace RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Data
{
    public static class CreateMessageDataSource
    {
        private static readonly List<object[]> validTestData
            = new List<object[]>
                {
                    new object[] { MessageDataWithOneRecipient() },
                    new object[] { MessageDataWithTwoRecipients() },
                    new object[] { MessageDataWithPriority()},
                    new object[] { MessageDataWithAttachments()},
                };

        private static readonly List<object[]> invalidTestData
            = new List<object[]>
                {
                    new object[] { MessageDataWithNoRecipient() },
                    new object[] { MessageDataWithInvalidRecipient() },
                    new object[] { MessageDataWithNoSender() },
                    new object[] { MessageDataWithInvalidSender() },
                    new object[] { MessageDataWithNoSubject()},
                };

        public static IEnumerable<object[]> ValidTestData
        {
            get { return validTestData; }
        }

        public static IEnumerable<object[]> InvalidTestData
        {
            get { return invalidTestData; }
        }

        private static MultipartFormDataContent MessageDataWithOneRecipient()
        {
            var data = new MultipartFormDataContent
            {
                { new StringContent("subject"), "Subject" },
                { new StringContent("content"), "Content" },
                { new StringContent("s@example.com"), "Sender" },
                { new StringContent("r@example.com"), "Recipients" }
            };
            return data;
        }

        private static MultipartFormDataContent MessageDataWithTwoRecipients()
        {
            var data = new MultipartFormDataContent
            {
                { new StringContent("subject"), "Subject" },
                { new StringContent("content"), "Content" },
                { new StringContent("s@localhost.pl"), "Sender" },
                { new StringContent("r1@example.com"), "Recipients" },
                { new StringContent("r2@example.com"), "Recipients" }
            };
            return data;
        }

        private static MultipartFormDataContent MessageDataWithPriority()
        {
            var data = new MultipartFormDataContent
            {
                { new StringContent("test with priority subject"), "Subject" },
                { new StringContent("test with priority content"), "Content" },
                { new StringContent("high"), "Priority" },
                { new StringContent("s@example.com"), "Sender" },
                { new StringContent("r@example.com"), "Recipients" }
            };
            return data;
        }

        private static MultipartFormDataContent MessageDataWithAttachments()
        {
            var data = new MultipartFormDataContent
            {
                { new StringContent("test with attachments subject"), "Subject" },
                { new StringContent("test with attachments content"), "Content" },
                { new StringContent("s@example.com"), "Sender" },
                { new StringContent("r@example.com"), "Recipients" },
                { new ByteArrayContent(Utilities.ExtractFileResource("lorem-ipsum-01.txt")), "Attachments", "lorem-ipsum-01.txt" },
                { new ByteArrayContent(Utilities.ExtractFileResource("lorem-ipsum-02.txt")), "Attachments", "lorem-ipsum-02.txt" }
            };
            return data;
        }

        private static MultipartFormDataContent MessageDataWithNoRecipient()
        {
            var data = new MultipartFormDataContent
            {
                { new StringContent("subject"), "Subject" },
                { new StringContent("content"), "Content" },
                { new StringContent("s@example.com"), "Sender" },
            };
            return data;
        }

        private static MultipartFormDataContent MessageDataWithInvalidRecipient()
        {
            var data = new MultipartFormDataContent
            {
                { new StringContent("subject"), "Subject" },
                { new StringContent("content"), "Content" },
                { new StringContent("s@example.com"), "Sender" },
                { new StringContent("recipient"), "Recipient" },
            };
            return data;
        }

        private static MultipartFormDataContent MessageDataWithNoSender()
        {
            var data = new MultipartFormDataContent
            {
                { new StringContent("subject"), "Subject" },
                { new StringContent("content"), "Content" },
                { new StringContent("r@example.com"), "Recipients" },
            };
            return data;
        }

        private static MultipartFormDataContent MessageDataWithInvalidSender()
        {
            var data = new MultipartFormDataContent
            {
                { new StringContent("subject"), "Subject" },
                { new StringContent("content"), "Content" },
                { new StringContent("sender"), "Sender" },
                { new StringContent("r@example.com"), "Recipients" },
            };
            return data;
        }

        private static MultipartFormDataContent MessageDataWithNoSubject()
        {
            var data = new MultipartFormDataContent
            {
                { new StringContent("content"), "Content" },
                { new StringContent("s@example.com"), "Sender" },
                { new StringContent("r@example.com"), "Recipients" }
            };
            return data;
        }
    }
}
