﻿using RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Data;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Controllers.Messages
{
    public class Create : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly CustomWebApplicationFactory<Startup> factory;

        public Create(CustomWebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
        }

        public void Dispose()
        {
            this.factory.FileStorageManager.ClearDirectoryStructure();
        }

        [Theory]
        [MemberData(nameof(CreateMessageDataSource.ValidTestData), MemberType = typeof(CreateMessageDataSource))]
        public async Task ReturnsSuccessCodeAndSavesSpecifiedAttachmentsWhenCreateMessageCommandIsValid(MultipartFormDataContent formDataContent)
        {
            var client = factory.CreateClient();
            var attachmentsCount = 0;

            foreach (var item in formDataContent)
            {
                if (item.GetType() == typeof(ByteArrayContent)) {
                    attachmentsCount++;
                }
            }

            var response = await client.PostAsync($"/api/messages", formDataContent);
            response.EnsureSuccessStatusCode();
            Assert.Equal(this.factory.FileStorageManager.GetAttachments().Count, attachmentsCount);
        }

        [Theory]
        [MemberData(nameof(CreateMessageDataSource.InvalidTestData), MemberType = typeof(CreateMessageDataSource))]
        public async Task ReturnsBadRequestWhenCreateMessageCommandIsNotValid(MultipartFormDataContent formDataContent)
        {
            var client = factory.CreateClient();
            var response = await client.PostAsync($"/api/messages", formDataContent);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
