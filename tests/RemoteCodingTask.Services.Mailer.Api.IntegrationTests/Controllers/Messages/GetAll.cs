﻿using RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Data;
using RemoteCodingTask.Services.Mailer.Application.Messages.Queries.GetMessagesList;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Controllers.Messages
{
    public class GetAll : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> factory;

        public GetAll(CustomWebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
        }

        [Fact]
        public async Task ReturnsCustomersListViewModel()
        {
            var client = factory.CreateClient();

            var response = await client.GetAsync("/api/messages");
            response.EnsureSuccessStatusCode();

            var respondeDto = await Utilities.GetResponseContent<MessageListDto>(response);

            Assert.IsType<MessageListDto>(respondeDto);
            Assert.NotEmpty(respondeDto.Messages);
            Assert.Equal(respondeDto.Messages.Count, SeedData.Messages.Count());
            Assert.All<Guid>(respondeDto.Messages.Select(vm => vm.Id), g => SeedData.Messages.Select(m => m.Id).Contains(g));
        }
    }
}
