﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Controllers.Messages
{
    public class Options : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> factory;

        public Options(CustomWebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
        }

        [Fact]
        public async Task ReturnsOkWithAllowHeader()
        {
            var supportedMethods = new string[] {
                                        HttpMethod.Get.ToString(),
                                        HttpMethod.Options.ToString(),
                                        HttpMethod.Post.ToString()
                                    };
            var client = factory.CreateClient();
            var request = new HttpRequestMessage(HttpMethod.Options, "/api/messages");
            var response = await client.SendAsync(request);

            Assert.Equal(System.Net.HttpStatusCode.OK, response.StatusCode);
            Assert.True(response.Content.Headers.Contains(HttpRequestHeader.Allow.ToString()));
            Assert.True(response.Content.Headers.TryGetValues(HttpRequestHeader.Allow.ToString(), out var allowHeaderValues));
            Assert.True(allowHeaderValues.All(item => supportedMethods.Contains(item)));
        }
    }
}
