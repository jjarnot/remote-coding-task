﻿using RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Data;
using RemoteCodingTask.Services.Mailer.ApplicationMessages.Queries.GetMessageDetail;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Controllers.Messages
{
    public class GetById : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> factory;

        public GetById(CustomWebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
        }

        [Fact]
        public async Task ReturnsCustomerViewModelWhenMessageIdIsValid()
        {
            var client = factory.CreateClient();

            var messageFromSeed = SeedData.Messages.First();

            var response = await client.GetAsync($"/api/messages/{messageFromSeed.Id}");
            response.EnsureSuccessStatusCode();

            var messageDetailsDto = await Utilities.GetResponseContent<MessageDetailsDto>(response);

            Assert.Equal(messageFromSeed.Id, messageDetailsDto.Id);
            Assert.Equal(messageFromSeed.Sender, messageDetailsDto.Sender);
            Assert.Equal(string.Join(";", messageFromSeed.Recipients), messageDetailsDto.Recipients);
        }

        [Fact]
        public async Task ReturnsNotFoundStatusCodelWhenMessageIdIsNotValid()
        {
            var client = factory.CreateClient();

            var invalidMessageId = Guid.Empty.ToString();

            var response = await client.GetAsync($"/api/messages/{invalidMessageId}");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
