﻿using Moq;
using RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Data;
using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using RemoteCodingTask.Services.Mailer.Application.Common.Models;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using RemoteCodingTask.Services.Mailer.Domain.ValueObjects;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Controllers.Messages
{
    public class Send : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> factory;
        private Mock<IMailer> mailerMock;

        public Send(CustomWebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
        }

        [Fact]
        public async Task SendsPendingMessages()
        {
            var client = factory.CreateClient();
            this.mailerMock = this.factory.MailerMock;
            this.mailerMock.Setup(x => x.SendMessage(It.IsAny<Email>(), It.IsAny<IList<Email>>(),It.IsAny<string>(), It.IsAny<string>(), It.IsAny<MessagePriority>(),It.IsAny<IList<AttachmentInfo>>()));

            var pendingMessagesBeforeSend = SeedData.Messages.Where(m => m.Status == Domain.Entities.MessageStatus.Pending).ToList();
            var response = await client.PostAsync("/api/messages/send", null);

            response.EnsureSuccessStatusCode();
            this.mailerMock.Verify(
                mock => mock.SendMessage(It.IsAny<Email>(), It.IsAny<IList<Email>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<MessagePriority>(), It.IsAny<IList<AttachmentInfo>>()), 
                Times.Exactly(pendingMessagesBeforeSend.Count));
        }
    }
}
