using RemoteCodingTask.Services.Mailer.Application.Common.Interfaces;
using RemoteCodingTask.Services.Mailer.Persistence;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using RemoteCodingTask.Services.Mailer.Api.IntegrationTests.Data;
using Moq;
using System.Linq;

namespace RemoteCodingTask.Services.Mailer.Api.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
        public Mock<IMailer> MailerMock { get; private set; }
        public IFileStorageManager FileStorageManager { get; private set; }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            this.MailerMock = new Mock<IMailer>(MockBehavior.Strict);

            builder
                .ConfigureServices(services =>
                {
                    RemoveAppDbContextRegistration(services);

                    // Create a new service provider.
                    var serviceProvider = new ServiceCollection()
                        .AddEntityFrameworkInMemoryDatabase()
                        .BuildServiceProvider();

                    // Add a database context using an in-memory 
                    // database for testing.
                    services.AddDbContext<MailerServiceDbContext>(options =>
                    {
                        options.UseInMemoryDatabase("InMemoryDbForTesting");
                        options.UseInternalServiceProvider(serviceProvider);
                    });

                    services.AddScoped<IMailerServiceDbContext>(provider => provider.GetService<MailerServiceDbContext>());
                    services.AddSingleton(this.MailerMock.Object);

                    var sp = services.BuildServiceProvider();
                    using var scope = sp.CreateScope();
                    var scopedServices = scope.ServiceProvider;
                    var context = scopedServices.GetRequiredService<MailerServiceDbContext>();
                    var logger = scopedServices.GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

                    this.FileStorageManager = scopedServices.GetRequiredService<IFileStorageManager>();

                    // Ensure the database is created.
                    context.Database.EnsureCreated();

                    try
                    {
                        SeedSampleData(context);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"An error occurred seeding the database with sample data. Error: {ex.Message}.");
                    }
                })
                .UseEnvironment("Test");
        }

        private static void RemoveAppDbContextRegistration(IServiceCollection services)
        {
            var descriptor = services.SingleOrDefault(
                d => d.ServiceType == typeof(DbContextOptions<MailerServiceDbContext>));

            if (descriptor != null)
            {
                services.Remove(descriptor);
            }
        }

        public static void SeedSampleData(MailerServiceDbContext context)
        {
            context.Messages.AddRange(SeedData.Messages);
            context.SaveChanges();
        }
    }
}
