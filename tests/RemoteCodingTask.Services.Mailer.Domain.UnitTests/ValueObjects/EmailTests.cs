﻿using RemoteCodingTask.Services.Mailer.Domain.ValueObjects;
using System;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Domain.UnitTests.ValueObjects
{
    public class EmailTests
    {
        [Fact]
        public void ImplicitConversionToStringResultsInCorrectString()
        {
            const string value = "example@example.com";

            var email = new Email(value);

            string result = email;

            Assert.Equal(value, result);
        }

        [Fact]
        public void ExplicitConversionFromStringSetsValue()
        {
            var email = (Email)"example@example.com";
            Assert.Equal("example@example.com", email.Value);
        }

        [Theory]
        [InlineData("someaddress")]
        [InlineData("email-example.com")]
        public void ShouldThrowArgumentExceptionForInvalidEmail(string invalidEmail)
        {
            Assert.Throws<ArgumentException>(() => (Email)invalidEmail);
        }
    }
}
