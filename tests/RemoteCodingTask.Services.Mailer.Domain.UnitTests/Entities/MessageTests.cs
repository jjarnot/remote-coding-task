﻿using RemoteCodingTask.Services.Mailer.Domain.Entities;
using RemoteCodingTask.Services.Mailer.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Domain.UnitTests.Entities
{
    public class MessageTests
    {
        [Fact]
        public void ShouldThrowArgumentExceptionInConstructorWhenIdIsGuidEmpty()
        {
            Assert.Throws<ArgumentException>(() 
                          => new Message(Guid.Empty, "s@test.local", new List<Email>() { "r1@test.local" },
                                         "subject-1", "content",
                                         MessageStatus.Pending, new DateTime(2020,1,1),null, MessagePriority.Normal));
        }

        [Fact]
        public void ShouldThrowArgumentExceptionInConstructorWhenRecipientsListIsEmptyEmpty()
        {
            Assert.Throws<ArgumentException>(()
                          => new Message(Guid.NewGuid(), "s@test.local", new List<Email>() {},
                                         "subject-1", "content",
                                         MessageStatus.Pending, new DateTime(2020, 1, 1), null, MessagePriority.Normal));
        }

        [Fact]
        public void ShouldThrowArgumentNullExceptionInConstructorWhenSubjectIsNull()
        {
            Assert.Throws<ArgumentNullException>(()
                          => new Message(Guid.NewGuid(), "s@test.local", new List<Email>() { "r1@test.local" },
                                         null, "content",
                                         MessageStatus.Pending, new DateTime(2020, 1, 1), null, MessagePriority.Normal));
        }

        [Fact]
        public void ShouldThrowArgumentExceptionInConstructorWhenSubjectIsEmpty()
        {
            Assert.Throws<ArgumentException>(()
                          => new Message(Guid.NewGuid(), "s@test.local", new List<Email>() { "r1@test.local" },
                                         "", "content",
                                         MessageStatus.Pending, new DateTime(2020, 1, 1), null, MessagePriority.Normal));
        }
    }
}
