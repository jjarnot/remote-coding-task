﻿using System;
using RemoteCodingTask.Services.Mailer.Domain.Entities;
using Xunit;

namespace RemoteCodingTask.Services.Mailer.Domain.UnitTests.Entities
{
    public class AttachmentTests
    {
        [Fact]
        public void ShouldThrowArgumentExceptionInConstructorWhenIdIsGuidEmpty()
        {
            Assert.Throws<ArgumentException>(() => new Attachment(Guid.Empty,"name","c:\\"));
        }

        [Fact]
        public void ShouldThrowArgumentNullExceptionInConstructorWhenNameIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new Attachment(Guid.NewGuid(), null, "c:\\"));
        }

        [Fact]
        public void ShouldThrowArgumentExceptionInConstructorWhenNameIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => new Attachment(Guid.NewGuid(), string.Empty, "c:\\"));
        }

        [Fact]
        public void ShouldThrowArgumentNullExceptionInConstructorWhenPathIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new Attachment(Guid.NewGuid(), "name", null));
        }

        [Fact]
        public void ShouldThrowArgumentExceptionInConstructorWhenPathIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => new Attachment(Guid.NewGuid(), "name", string.Empty));
        }
    }
}
